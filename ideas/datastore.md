# datastore

The whole point is to use the event stream to react and produce materialisd views, so that you have an OLTP and OLAP system in one.
This way you can do transactions and reporting from the same base framework.

1. Dont need scripting, as this is not a DB where you write queries on top.
- typically you want to hit an index that has been created to get back a list of ID's of documents or rows.
- then you get your data from the datastore by ID, and then run the final basic query on it and sort them as needed.

2. ETL (extract transform load). This type of thinkig is more suited

3. RBAC. Not need because if the client has the data (via the Client / Server event pump) they are already authourised.
- Server side all operations have RBAC applied at the API level.

4. Works in a browser or server. SO indexdb it is then.

Graphql Looks like a good fit.
https://github.com/neelance/graphql-go
- great implemenation
- graphql premsie is that you can query many datastores which is exactly what we need.

https://github.com/playerx/architecture-graphql-cqrs-eventsourcing-redux

https://gist.github.com/OlegIlyenko/a5a9ab1b000ba0b5b1ad
- https://github.com/sangria-graphql/sangria-subscriptions-example
BTW its a kapp architecture in reality and i agree.
- https://github.com/blacklabeldata/kappa

https://github.com/reimagined/resolve/
- This really sums up what needs to be done well.
- Also uses code gen, and so the actual amont of code is very small.
- Great example and docs !! https://github.com/reimagined/hacker-news-resolve

## data migrations
this one is hard because all your brwosers also have a database :)
- Must use a protobuf so that version diffences in struc fields dont blow up.
- Never bring down a service, but always make a new one. Weird i knwo but works and is done at googel i beleive. So version your services.
- Put any data migration sripts into the data stream itself. Not sure.. Just idea.





