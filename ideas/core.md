# core archi

Taking a realist view this is the best Roadmap

https://github.com/johnpryan/redux.dart
- looks like the offical adopted way to do reflex for flutter and web / angular.
Follow it. looks nice

https://github.com/johnpryan/grpc-on-heroku
- golang grpc and dart . Exactly what we need.
- uses http,not http2. Dart only needs to knoew json


https://github.com/jban332/flutter2js
- WOW !!!!

https://github.com/franzsilva/flutter_launcher_icons
- fixed for IOS. Try it and close the issue

https://github.com/skelterjohn/go.wde
- Desktop code for pretty much everyting i need.





GUI
- Flutter for mobile.
	- webviews:
		- github.com/dart-flitter/flutter_webview_plugin
		
- Angular for web
- Or Make flutter work for desktop ?
	- Rememebr this ALSO solves the Printing problem !!!
	- Remote the flutter screen with RDP
		- https://github.com/dart-lang/sdk/issues/31547
		- https://github.com/flutter/flutter/issues/11648
			- Captures a users Signature out to a Image. Shows the Concept.
		- https://stackoverflow.com/questions/41957086/creating-raw-image-from-widget-or-canvas
			- Shows Concpe of screen capture, but then also Printing
	- Run Flutter on a server virtualsied
		- Anbox with RDP
- Now how to reuse huge amounts of code between them ?
	- Use built-value and codegen from a JSON RPC API with websockets also.
- Run a golang "Server" onthe mobile
	- Centrifugo has relevant code: https://github.com/centrifugal
		- Thank gode someone is really doing it.
		- Its a basis.
		- Decide is DART / Flutter can handel GRPC !
- CRDT offline etc
	- GUI indepent, we want the gui to NOT control this.
	- Hashicorp serf looks VERY promisng.
		- Raft is for Servers and does concensus by electing a LEADER
			- This enforces consistency to be ALWAYS true
			- Is uses a replicatd Log
		- Serf is for Clients and does it by gossiping to the Servers
			- THis enfocres consistency through eventual consistency, so each client can have a different state from others, but eventually all will be consistent.
		- https://github.com/docker/libnetwork
			- This uses Serf and can model a datastore to be eventualy consistent
		- https://github.com/docker/swarmkit
			- This uses Raft to make all Servers consistent
		- https://github.com/celrenheit/sandglass
			- uses hashicorf serf & raft
			- has pub sub broker
			- has badger db which i presume can be distributed
		- https://github.com/gritzko/ron
			- client code is js. yuck: https://github.com/gritzko/swarm
- Pub Sub COntrol plane
	- https://github.com/muesli/beehive
		- simple and configurable.
	- https://github.com/Jeffail/benthos
	- Nats of course
	- https://github.com/lileio/lile
		- generates everything need to use grpc
		- very complete and good docs

- Modules
	- Facet Search
		- I want this because its so powerful for all use case.
		- CODE !!!! https://bitbucket.org/ouava/ducene/src
			- Dart code that really shows it working !!



Server
- Golang Server running on mobiles, desktops and server. 
	- Golang Mobile Starter: https://github.com/gokitter/kitter/blob/master/Makefile
	- Transport channels: https://github.com/matryer/vice
- This allow offline, where the golang server runs as a service on all devices. 
	- On client devices its more like a cache
	- Use metrics or Pinnn to decide whats gets evidcted
- CQRS like system
	- Sets via the API provided by the Module
	- Gets via a Graphql API. Implies dgraph 
	- Orchestration via simple NATS liek thing, BUT lets NOT do microservices, but instead keep it all as one golang process as much as possibel fro now.
- Transport Layer
	- JSON RPC
	- Golang side
		- https://blog.twitch.tv/twirp-a-sweet-new-rpc-framework-for-go-5f2febbf35f
		- https://github.com/twitchtv/twirp
			- code generates the HTTP Handler per module. Nice and easy.
	- Dart side
		- Dart loves to use JSON RPC so should be easy.
		- GUI does NOT maintain any state, so we dont need reflux and all that. Instead it has a local golang DB and so whenever a view opens it loads each time.
			- Whilst view is open is can if its wants tell the golang to keep a websocket open asking for change feeds.
- Orchestration Layer
	- NATS with codegen on top
		- https://github.com/chop-dbhi/nats-rpc
	- Research "Synadia nats"


Security
- Auhtorization & Access control is via policy files using golang RBAC
	- The big oen everyone uses is : https://github.com/casbin/casbin
	- Mapped against the API. We dont need data level access control. Whatever the API provides is what users have access to.
- Authentication
	- some sort of single sign on that is global so we have a presense system.
	- Notary has everthign we need.
		- Auth 
		- Keys
		- Signing data and encrypting it.
- Wire Crypto
	- VPN 


Sync
- All documents are simple json docs.
- Lamport clocks to control mutation ordering. 
	- See Alex's code: https://github.com/influx6/faux/tree/master/lpclock
	- Hashicorps: https://www.serf.io/docs/internals/gossip.html
- NAT / Service Discovery
	- Need a distributed endpoint system also that self replicated so everyones changeing NATS dont screw everything up.
	- https://github.com/weaveworks/mesh
		- Pretyt obviosu one to use

Search
- Bleve with boltdb. DGraph might get their shit togther.
	- Or Blevel with Badger if the get the snapshotting working.
- GUI: Angular is here: https://bitbucket.org/ouava/ducene/src
	- We can easily make a Flutter one i think.
- Search can be per App, Or system wide for Managers and analytics.

Packaging
- Lantern and Zserge has most of what we need.


----

Foundation Libraies
https://www.cncf.io/projects/


---

An examle like Max's docs
- Use case is a CMS like thing:
	- Content is in markdown. These Markdown files are our mutations.
- Flow:
	- Server
		- Mutation of each page sent in.
		- Then page sent to Hugo geneates the page and its assets, and its PUBS "New page"
		- Then via SUB of "New Page" the page is sent to Indexing. Bleve for indexing, and its PUBS "New Page index"
		- Then via SUB of "New Page" the page sent to Storage. Maybe Dgrpah for storage or minio, and it PUBS "New Page stored"
	- GUI uses the API that already been written we presume.
		- Search via free text and facets.
		- Edit use Dart Markdown, and patches are sent back into the Flow.

- Development
If we use a microservices K8 pattern can be develop faster ?
can we use a web IDE

- Company website
	- Copy Hashicorp	
		- Main site has a MegaNav which links to sub projects, and so this allows us the scalability we need as we introduce Modules.
