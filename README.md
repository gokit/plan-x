# plan-x

## Intent ?
Browsers & Webviews to run everything.
Golang can compile to JS ( and soon WASM). SO why not match the Browser API's for all the things we use serverside ?
This will alow isomorphic development where you compile a complex app to run Client side and then also run parts of it server side.
Why types of things ?
- The DB. SO indexDB wince that works on every browser
- The Pub Sub. So Service worker 
- The MicroServices. SO web worker. After all its using PostMessage, which looks alot like NATS, etc

Offline first.
Basically this goes hand in hand with the 1st goal. If oyu want to code a complex app to be offline first you needs lots of stuff that normally will be running server side.
Currently CRDT is the main approahc with simple document style types.

CQRS Pub Sub.
Use BLeve and pilosa with the IndexDB and pipeline it so you get all the great things of cqrs, and not have to code a flux pattern into your GUI.
This is a bit hard to explain.
TODO: Add more meat on the bones here.

Update without needing any appstore. 
If you can run everything inside a webview than its pretty easy to update. 
It also simplifies lots of OPs and other complexities to do with multiversioning, but you still have to design for it.
    - For example rather than change an existing module that has gone into production you actually copy the code and write a new one. Sounds nuts but its effective because then different clients can run different versions of the Modules and not clash.


## Reuse
Gokit has lots of great layers that can be reused, so the idea is to use it and formulate it to match the 

## Stack

First thing is to use MDNS so give you a LAN network by which you can see computers on the network so you get a basic test envonment because your going to be running on many emulated mobiles and desktops.

Will add links later, but cant right now as no decent connection here :)

### Containers
- For Mobile we use Flutter webview.
    - https://github.com/apptreesoftware/flutter_webview
        - OLD
    - https://github.com/dart-flitter/flutter_webview_plugin
        - Much better because it allows lots of the things we need like fullscreen, partial screen and hidden screen.
        - Can do comms between the gopherjs and Dart which is key.
- For Desktop use the zserge webview.
    - https://github.com/zserge/webview
        - This has 99% of what we need.
        - Does no support printing.

### Container Services.
- Printing
    - Flutter: https://github.com/Rockvole/pdfview
        - Then push the PDF to a share. Gotta tr it.
    - Desktop: Zserge does not provide this, but if we have isomorphic rendering then we can render using the Server (runing onthe desktop)
        - Do a HTML to PDF with various cross platform stuff that runs on all desktops.
        - Then push the PDF to the printer using Command.exec to the OS print queue. Hunt aroudn for a examples.
- Menus / back buttons, etc
    - Flutter: Has this baked in to the Material Design lib and we can drive it from the webview layer.
    - Desktop: We dont need much because its backed into the GUI.
- Scheduling.
    Flutter: Use the Alarm Manager and the Plugin Registry. This means you dont have to write background services to get work done. From the webview
    Desktop: We can use normal golang scheduling, and not gopherjs stuff.
- File / Folder opening
    Flutter: Dont knwo yet, but it wil be an intent
    Desktop: ZSerge has this backed in an its really nice.


### CI, CD
- Packaging
- For submission to app stores you need to package the right way.
    - Flutter: Not much to do here as they did a ton of work for us.
    - Desktop: 
        - OSX is easy.
- Updating
    - See Lantern code base.
    - Because its all inside a webview and your data is there, database changes need to be pat of the ordered event stream. Make ops easy
- Icon gen:
    - Flutter: Has plugin for it.
    - Desktop: Not sure.

### GUI
- Use Material Design components.
    - This has really come of age now and is fast and very high quality. RTL 

### Control Plane / Fluxer
- This is a basic pub sub control plane, and so we dont need reflux stuff
- Its needs it own DB: use indexdb in browser and leveldb server side. Both are the same DB API. 
- Its needs its own thread. Use web workers as this is a Message Passing system anyway.
- UUID, so each message is truely unique. Sand

### Event Stream.
- You need the types to be eventually consistent. Y.js looks strong.
    - https://github.com/y-js/yjs/issues/63
    - This is CRDT, and so is Clock independent. As you can see you can build document style systems easily. Thsi is why we use bleve and pilosa for indexign and searching ! We dotn hae SQL and its stinks anyway.
- For now i suggest building golang types that match y-js based types using Struct embededding
- For sockets between Clients and Servers look at:
    - https://github.com/libp2p/js-libp2p-websocket-star/blob/master/README.md
- https://github.com/GetStream/stream-go2
    - Nice abstraction


### Services
- These run in their own web workers
- These can be built up as user modules for whatver functionality you need.
- But underlying ones we need are:
    - IndexDB is one for storage
    - Bleve is one for unstructured text indexing
    - Pilosa is another for structured types indexing.
- This all can run as gopherJS. I saw bleve can do it, and it just needs a driver adjustment so its used the indexdb (or leveldb server side).


Below this your basically hitting the server, which is exactly the same architecture compiled for golang.
- The client backend subscribes to the Server-frontend ! thats all it is and its the same code, but with no logic.  
- Dont forget about security here.

### Security
- Add layer using obvious RBAC policy files.




## Conceptual Roadmap

1. CI, CD
- You need the CI and CD setup and working.
- This means the Containers and Packaging updating is also working. 

2. Demo
- This combines the layers to test the asserts of the architecture.
- Start with the MDC and GUI inside webviews talkign to server and then work backwards putting more and more into gopherjs.

