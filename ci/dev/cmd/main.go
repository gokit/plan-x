package main

import (
	"log"

	"github.com/magefile/mage/mg"
	//"github.com/gedw99/01/ci/dev/pkg"
)

var (
	commitHash string
	timestamp  string
	gitTag     = "v2"
)

// Runs go install for gnorm.  This generates the embedded docs and the version
// info into the binary.
func Build() error {
	mg.Deps(installHugo, installStatik)
	if err := genSite(); err != nil {
		return err
	}
	defer cleanup()

	ldf, err := flags()
	if err != nil {
		return err
	}

	log.Print("running go install")
	// use -tags make so we can have different behavior for when we know we've built with mage.
	return run("go", "install", "-tags", "make", "--ldflags="+ldf, "gnorm.org/gnorm")
}
