# Dev

This uses Mage so that build and CI all uses golang and so we get reuse between projects and CI.

Put all packages here for doing the variosu things you need for a project.

Then a project can reference dev package and get access to all the functionality.
SO the developer can write a mage.go file and becaue its go the get all the functionality via a package.
This is much easier than screwing acround with Makefile inheritance, sh scripts. Thats fine for a few projects but when you have many its gets silly.


Things:
- Boot strapping a machine
	- calling brew
	- setting the $HOME/.bash profile
	- checking that certain ENV variables and binaries exist on the path.
		- can vary by OS too, and even CI.

- Packaging for mobile and desktop and web

- Icon and splash screen generation

- hugo docs with search
	- and for embeddign them into a binary

- CI stuff.

- test stuff.