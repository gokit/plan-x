package hugo

import (
	"log"
)

func installHugo() error {
	log.Print("downloading hugo")
	return run("go", "get", "github.com/gohugoio/hugo")
}

func installStatik() error {
	log.Print("downloading statik")
	return run("go", "get", "github.com/rakyll/statik")
}
