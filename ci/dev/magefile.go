// +build mage

package main

import (
	"os"
	"time"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

var (
	packageName = "github.com/kevingimbel/license"
	LDFLAGS     = "-X $PACKAGE/lib.version=$VERSION -X $PACKAGE/lib.buildDate=$BUILD_DATE"
)

// Proves a flag environment for LDFLAGS
func flagEnv() map[string]string {
	return map[string]string{
		"PACKAGE":    packageName,
		"BUILD_DATE": time.Now().Format("2006-01-02T15:04:05Z0700"),
		"VERSION":    "1.0.0",
	}
}

// gen icons

// run many processes for dev.

// allow user to override go executable by running as GOEXE=xxx make ... on unix-like systems
var goexe = "go"

func init() {
	if exe := os.Getenv("GOEXE"); exe != "" {
		goexe = exe
	}
}

func getDep() error {
	return sh.Run(goexe, "get", "-u", "github.com/golang/dep/cmd/dep")
}

// Install Go Dep and sync Hugo's vendored dependencies
func Vendor() error {
	mg.Deps(getDep)
	return sh.Run("dep", "ensure")
}

// Run tests
func Test() error {
	return sh.Run("go", "test")
}

/*
func Screenshot() error {
	capture.BrowserScreenshot()
	return sh.Run("go", "test")
}
*/

// Builds the binary
func Build() error {
	return sh.RunWith(flagEnv(), "go", "build", "-ldflags", LDFLAGS, packageName)
}

// Create a full release with goreleaser (Pushes to GitHub)
func Release() error {
	return sh.Run("goreleaser", "--rm-dist")
}

// Create a pre-release with goreleaser
func PreRelease() error {
	return sh.Run("goreleaser", "--rm-dist", "--snapshot")
}
