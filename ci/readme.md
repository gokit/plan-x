# cli

There are two types:
- dev
	- this is at developer time and does things list icons etc
- build
	- this is CI and CN in the cloud.

## app

This is a binary in your gopath to do lots of things you need alot.
You can kick it of manually or from your build scripts.

Making the build script golang using Mage makes it easier to reuse in the CI.
Or maybe bitrise steps ?

TODO:
Find a way to use Mage to run many apps at once (ike tmux). This is needed for dev time stuff.



https://github.com/magefile/mage
- Mage allows me to use the same code for all CI
- has hugo with integrated search !
- has goleaser !
- users:
	- https://github.com/gohugoio/hugo/blob/master/appveyor.yml

Things:
- Icon generation
	- github.com/franzsilva/flutter_launcher_icons 
- ios and andorid management
	- https://github.com/bitrise-tools/go-android
	- https://github.com/bitrise-tools/codesigndoc
	- https://github.com/bitrise-tools/go-xcode


testing with fastlane

CLI helpers to make it all easier
github.com/desertbit/grumble
- cli and shell in one.

