# golang

what do we need ?

Server stack thats simple and fast with graph db
Must support roles that admin manipulates
- casbin for security
- echo for baic web gui stuff
- dgraph for server and db in one.

GUI
-  Its an SPA and all in Dart with Angular or similar.

We need to run the web Ui on a dekstop.
Webview for desktop
https://github.com/zserge/webview
- has everything we need and well supported.
- ant print yet though.

Packaging for every Desktop OS
https://github.com/naivesound/glitch
- Has all scripts needs. Just change as needed.

Proxy. We need an easy way for mobiles to reach a proxy with non static iP.
https://github.com/google/inverting-proxy
	- This uses an Inverted Proxy running on AppEngine Instance open to the world.
	- In the office is the agent and the normal backend.
	- The agent calls the Inverted Proxy, and the Inverted proxy holds the session open.
	- Now, when calls come in the the outside world to the Inverted Proxy, it forwards the request to the Agent which forwards it to the backend. The bakends sends the response and it all gets pushed out to the Client.
	- Auth Security is applied at the Inverted Proxy using the Users ID Or a key. This can be secured with HTTPS. 
	- If you want to use websockets, you need to use socket.io
	- Everything is HTTP. No HTTPS.

Printing.
Stuck on this one. I could just make PDF Templates. This can work on Desktop and Flutter.
For now its really the only option.
Once the PDF is generated then push it at the printer queue using host specific calls via Command.exec