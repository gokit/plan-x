// Code generated with goa v2.0.0-wip, DO NOT EDIT.
//
// HTTP request path constructors for the sommelier service.
//
// Command:
// $ goa gen goa.design/plugins/goakit/examples/cellar/design

package server

// PickSommelierPath returns the URL path to the sommelier service pick HTTP endpoint.
func PickSommelierPath() string {
	return "/sommelier"
}
