// Code generated with goa v2.0.0-wip, DO NOT EDIT.
//
// swagger endpoints
//
// Command:
// $ goa gen goa.design/plugins/goakit/examples/cellar/design

package swagger

type (
	// Endpoints wraps the swagger service endpoints.
	Endpoints struct {
	}
)

// NewEndpoints wraps the methods of a swagger service with endpoints.
func NewEndpoints(s Service) *Endpoints {
	return &Endpoints{}
}
