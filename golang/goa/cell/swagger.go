package cellar

import (
	"github.com/gedw99/gitty/exp/services/gokit/cell/gen/swagger"
	"github.com/go-kit/kit/log"
)

// swagger service example implementation.
// The example methods log the requests and return zero values.
type swaggersvc struct {
	logger log.Logger
}

// NewSwagger returns the swagger service implementation.
func NewSwagger(logger log.Logger) swagger.Service {
	return &swaggersvc{logger}
}
