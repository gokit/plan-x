package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/blang/semver"
	"github.com/rhysd/go-github-selfupdate/selfupdate"
)

const version = "2.3.0"

const usageHeader = `USAGE: github-clone-all [FLAGS] {query}

  github-clone-all is a command to clone all repositories matching to given
  query via GitHub Search API. Query must not be empty.
  It clones many repositories in parallel.

  Repository is cloned to 'dest' directory. It is $cwd/repos by default and
  can be specified with -dest flag.

  All arguments in {query} are regarded as query.
  For example,

  $ github-clone-all foo bar

  will search 'foo bar'. But quoting the query is recommended to avoid
  conflicting with shell special characters as following:

  $ github-clone-all 'foo bar'

  Because of restriction of GitHub search API, max number of results is 1000
  repositories. And you may need to gain GitHub API token in advance to avoid
  reaching API rate limit.

  `

func usage() {
	fmt.Fprintln(os.Stderr, usageHeader)
	flag.PrintDefaults()
}

func selfUpdate() int {
	v := semver.MustParse(version)

	latest, err := selfupdate.UpdateSelf(v, "rhysd/github-clone-all")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return 3
	}

	if v.Equals(latest.Version) {
		fmt.Println("Current version", v, "is the latest")
	} else {
		fmt.Println("Successfully updated to version", v)
		fmt.Println("Release Note:\n", latest.ReleaseNotes)
	}
	return 0
}

func main() {
	help := flag.Bool("help", false, "Show this help")
	h := flag.Bool("h", false, "Show this help")

	ver := flag.Bool("version", false, "Show version")
	update := flag.Bool("selfupdate", false, "Update this tool to the latest")

	flag.Usage = usage
	flag.Parse()

	if *help || *h {
		usage()
		os.Exit(0)
	}

	if *ver {
		fmt.Println(version)
		os.Exit(0)
	}

	if *update {
		os.Exit(selfUpdate())
	}

}
