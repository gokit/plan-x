/*

Financial Cals used for modelling any business.

Golang copies of numpy's finance functions
https://docs.scipy.org/doc/numpy/reference/routines.financial.html



*/

package main

import (
	"fmt"

	f "github.com/orcaman/financial"
	/*
		"github.com/orcaman/financial"
	*/)

func main() {

	IRR()

	fmt.Println("")

	NPV()

}

// IRR ...
func IRR() {

	// IRR
	initialInvestment := 100.0

	cashFlowYear1 := 39.0
	cashFlowYear2 := 59.0
	cashFlowYear3 := 55.0
	cashFlowYear4 := 20.0

	irr, err := f.IRR([]float64{-initialInvestment, cashFlowYear1, cashFlowYear2, cashFlowYear3, cashFlowYear4})
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Printf("IRR is: %f", irr)

}

// NPV ...
func NPV() {
	//NPV

	initialInvestment := 100.0

	cashFlowYear1 := 39.0
	cashFlowYear2 := 59.0
	cashFlowYear3 := 55.0
	cashFlowYear4 := 20.0

	discountRate := 0.281

	npv, err := f.NPV(discountRate, []float64{-initialInvestment, cashFlowYear1, cashFlowYear2, cashFlowYear3, cashFlowYear4})
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Printf("NPV is: %f", npv)
}
